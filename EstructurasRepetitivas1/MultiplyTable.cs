﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Volem printar les taules de multiplicar.

L'usuari introdueix un enter

Mostra per pantalla la taula de multiplicar del número introduït:
*/
namespace EstructurasRepetitivas1
{
    public class MultiplyTable
    {
        public void latablaa() 
        {
            /*MultiplyTable

            */

            int multiplicalo = Convert.ToInt32(Console.ReadLine());
            int i = 0;
            while (i <= 10)
            { 
                Console.WriteLine($"{i} x {multiplicalo} = {i*multiplicalo}");
                i++;
            }
            // millor amb for perqué es una condició definida 

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Compta les linies fins que l'usuari introdueixi END
*/
namespace EstructurasRepetitivas1
{
    public class HowManyLines
    {
        public void Liness ()
        {
            string textaco = "";
            int tantas = 0;
            // do while
            while (textaco != "END"){
                textaco = Convert.ToString(Console.ReadLine());
                tantas++;
            }
            Console.WriteLine($"Tu texto tiene {tantas-1} linias"); 

        }
    }
}


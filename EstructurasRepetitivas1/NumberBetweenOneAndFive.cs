﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: 
*/

namespace EstructurasRepetitivas1
{
    public class NumberBetweenOneAndFive
    {
        public void NumberBTW()
        {
            int numero = 0;

            do
            {
                numero = Convert.ToInt32(Console.ReadLine());
                if (numero < 1 || numero > 5)
                {
                    Console.WriteLine("Sigue intentando el número tiene que ser mayor a 1 y menor a 5"); 
                }
            } while (numero < 1 || numero > 5);
            Console.WriteLine($"El número introduït :{numero}"); 

        }
    }
}

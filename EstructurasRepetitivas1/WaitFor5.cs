﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Compta les linies fins que l'usuari introdueixi END
*/
namespace EstructurasRepetitivas1
{
    public class WaitFor5
    {
        public void Wait5()
        {

            int cincoo = 0; 
            while (cincoo != 5)
            {
                cincoo = Convert.ToInt32(Console.ReadLine());
                if (cincoo == 5)
                {
                    Console.WriteLine("5 trobat ! ");
                } else {
                    Console.WriteLine("Sigue intentando crack : "); 
                        
                       }

            }

        }

    }
}

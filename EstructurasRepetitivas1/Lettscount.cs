﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
*/
namespace EstructurasRepetitivas1
{
    public class LetsCount 
    {    

        public void LettsCount()
        {
            int enter = Convert.ToInt32(Console.ReadLine());
            int numero = 0;

            while (numero <= enter)
            {
                Console.WriteLine(numero);
                numero++;
            }
        }
    }
}

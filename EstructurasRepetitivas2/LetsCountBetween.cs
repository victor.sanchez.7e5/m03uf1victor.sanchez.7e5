﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Compta les linies fins que l'usuari introdueixi END
*/

namespace EstructurasRepetitivas2
{
    public class LetsCountBetween
    {
         public void LetsCountt()
         {
            int numx = 0; 
            int numy = 0;
            int elpequeño= 0;
            int elgrande = 0; 

            numx = Convert.ToInt32(Console.ReadLine()); 
            numy = Convert.ToInt32(Console.ReadLine());

            if (numx> numy)
            {
                elpequeño = 1+numy;
                elgrande = numx; 
            }else
            {
                elpequeño = 1+numx;
                elgrande = numy; 
            }
                        
            for (; elpequeño < elgrande; elpequeño++)
            {
                Console.Write(elpequeño+" "); 
            }

        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Printa per pantalla tants punts com el número indicat per l'usuari .
*/
namespace EstructurasRepetitivas2
{
    public class DotLine
    {
        public void Dott()
        {
            Console.WriteLine("Escriu el número de punts que vols per pantalla"); 
            int los = Convert.ToInt32 (Console.ReadLine());
            int count= 0;
            
            // no tiene mucho sentido en realidad hacerlo con while :=)
            /*do
            {
                Console.Write(".");
                count++;
            } while (count < los);  */

            for (; count < los; count++)
            {
                Console.Write("."); 
            }

            Console.WriteLine($"Ahi tienes {los} puntos");

        }
    }
}

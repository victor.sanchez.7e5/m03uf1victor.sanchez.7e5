﻿
using EstructurasRepetitivas2;

internal class Program
{
    private static void Main(string[] args)
    {

        Console.WriteLine("ESTRUCTURES REPETITIVES 2");


        Console.WriteLine(" 1 - DotLine");
        Console.WriteLine(" 2 - LetsCountBetween");
        Console.WriteLine(" 3 - CountDown");

        string exercici = Convert.ToString(Console.ReadLine());


        switch (exercici)
        {
            case "1":
                Console.WriteLine(" DotLine");
                DotLine dt = new DotLine();
                dt.Dott(); 
                break;

            case "2":
                Console.WriteLine("LetsCountBetween");
                LetsCountBetween lc = new LetsCountBetween();
                lc.LetsCountt();
                break;

            case "3":
                Console.WriteLine("CountDown");
                CountDown cd = new CountDown();
                cd.Countt();
                break; 
        }


    }
}
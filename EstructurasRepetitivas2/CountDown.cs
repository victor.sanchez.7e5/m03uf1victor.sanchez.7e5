﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Fem una comta enrere a partir del número que introduirà l'usuari. 
*/
namespace EstructurasRepetitivas2
{
    public class CountDown
    {
        public void Countt()
        {
            int n = 0;
            Console.WriteLine("Escriu un nombre a partir del cual farem una comta enrere : "); 
            n = Convert.ToInt32(Console.ReadLine()); 

            for (n--; n >= 1; n--)
            {
                Console.WriteLine(n);
            }
        }
    }
}

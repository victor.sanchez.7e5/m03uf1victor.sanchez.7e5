﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Compta les linies fins que l'usuari introdueixi END
*/

namespace EstructurasRepetitivas3
{
    public class CountWithJumps
    {
        public void Jumps()
        {
            Console.WriteLine("L'usuari introdueix dos valors enters, el final i el salt");
            Console.WriteLine("Valor final :");
            int final = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Valor de salt :");
            int salt = Convert.ToInt32(Console.ReadLine()); 

            for (int n = 1; final >= n;)
            {
                Console.WriteLine(n+" ");
                n += salt; 
            }
        }
    }
}



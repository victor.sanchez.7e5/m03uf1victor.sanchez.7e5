﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
Author: Víctor Sánchez
Data: 25/10/2022
Description: Compta les linies fins que l'usuari introdueixi END
*/

namespace EstructurasRepetitivas3
{
    public class ManualPow
    {
        public void ManualPw()
        {
            Console.WriteLine("Escriu un enter Base");
            int b = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Escriu un enter Base");
            int e = Convert.ToInt32(Console.ReadLine());

            int resultats = 1; 

            for (int i =0; i < e; i++)
            {
                resultats *= b;
            }

            Console.WriteLine($"La potència amb base {b} i exponent {e} és igual a {resultats}");
        }
    }
}

﻿using EstructurasRepetitivas3;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("ESTRUCTURES REPETITIVES 3");


        Console.WriteLine(" 1 - CountWithJumps");
        Console.WriteLine(" 2 - OnlyVowels");
        Console.WriteLine(" 3 - GoTournamentGreatestScore");


        string exercici = Convert.ToString(Console.ReadLine());


        switch (exercici)
        {
            case "1":
                Console.WriteLine(" CountWithJumps");
                CountWithJumps ctj = new CountWithJumps();
                ctj.Jumps();
                break;

            case "2":
                Console.WriteLine(" OnlyVowels");
                OnlyVowels ov = new OnlyVowels();
                ov.Vowel(); 
                
                break;

            case "3":
                Console.WriteLine("GoTournamentGreatestScore");
                GoTournamentGreatestScore gt = new GoTournamentGreatestScore();
                gt.GreatestScore(); 
                break; 
            case "4":
                Console.WriteLine("Manual Pou (potencias)");
                ManualPow mp = new ManualPow();
                mp.ManualPw(); 
                break;
            case "5":
                SumMyNumbers sn = new SumMyNumbers();
                sn.SumNumbers();
                break; 
        }

    }
}


